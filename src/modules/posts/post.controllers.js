import Post from './post.model';
import User from '../users/user.model';

export async function createPost(req, res) {
  try {
    const post = await Post.createPost(req.body, req.user._id);

    return res.status(201).json(post);
  } catch (e) {
    return res.status(400).json(e);
  }
}

export async function getPostById(req, res) {
  try {
    const promise = await Promise.all(
      User.findById(req.user._id),
      Post.findById(req.params.id).populate('user'),
    );

    const favorite = promise[0]._favorites.isPostFavorite(req.params.id);
    const post = promise[1];

    return res.status(200).json({
      ...post.toJSON(),
      favorite,
    });
  } catch (e) {
    return res.status(400).json(e);
  }
}

export async function getPostList(req, res) {
  const limit = parseInt(req.query.limit, 0);
  const skip = parseInt(req.query.skip, 0);
  try {
    const posts = await Post.list({ limit, skip });
    return res.status(200).json(posts);
  } catch (e) {
    return res.status(400).json(e);
  }
}

export async function updatePost(req, res) {
  try {
    const post = await Post.findById(req.params.id);

    if (!post.user.equals(req.user._id)) {
      return res.sendStatus(401);
    }

    Object.keys(req.body).forEach(key => {
      post[key] = req.body[key];
    });

    return res.status(200).json(await post.save());
  } catch (e) {
    return res.status(400).json(e);
  }
}

export async function deletePost(req, res) {
  try {
    const post = await Post.findById(req.params.id);

    if (!post.user.equals(req.user._id)) {
      return res.sendStatus(401);
    }

    await post.remove();
    res.sendStatus(200);
  } catch (e) {
    res.status(400).json(e);
  }
}

export async function favoritePost(req, res) {
  try {
    const user = await User.findById(req.user._id);

    await user._favorites.posts(req.params.id);

    return res.sendStatus(200);
  } catch (e) {
    return res.status(400).json(e);
  }
}
