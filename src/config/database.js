/* eslint-disable no-console */
import mongoose from 'mongoose';

import constants from './constants';

// Remove warning with Promise
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);

// Connect the db with url provided
try {
  mongoose.connect(constants.MONGO_URL, {
    useNewUrlParser: true,
    useFindAndModify: false,
  });
} catch (err) {
  mongoose.createConnection(constants.MONGO_URL);
}

mongoose.connection
  .once('open', () => console.log('MongoDB Running'))
  .on('error', e => {
    throw e;
  });
